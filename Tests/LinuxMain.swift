import XCTest
@testable import LightweightTestSuite

XCTMain([
     testCase(LightweightTests.allTests),
])
